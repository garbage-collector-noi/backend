PGDMP                      
    y            GarbageCollector    13.3    13.3 T               0    0    ENCODING    ENCODING        SET client_encoding = 'UTF8';
                      false                       0    0 
   STDSTRINGS 
   STDSTRINGS     (   SET standard_conforming_strings = 'on';
                      false                       0    0 
   SEARCHPATH 
   SEARCHPATH     8   SELECT pg_catalog.set_config('search_path', '', false);
                      false                        1262    27511    GarbageCollector    DATABASE     m   CREATE DATABASE "GarbageCollector" WITH TEMPLATE = template0 ENCODING = 'UTF8' LOCALE = 'German_Italy.1252';
 "   DROP DATABASE "GarbageCollector";
                postgres    false            �            1259    27586 	   bin_tours    TABLE     d   CREATE TABLE public.bin_tours (
    id integer NOT NULL,
    bin_id integer,
    tour_id integer
);
    DROP TABLE public.bin_tours;
       public         heap    postgres    false            �            1259    27584    bin_tours_id_seq    SEQUENCE     �   CREATE SEQUENCE public.bin_tours_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 '   DROP SEQUENCE public.bin_tours_id_seq;
       public          postgres    false    213            !           0    0    bin_tours_id_seq    SEQUENCE OWNED BY     E   ALTER SEQUENCE public.bin_tours_id_seq OWNED BY public.bin_tours.id;
          public          postgres    false    212            �            1259    27522    bincontents    TABLE     t   CREATE TABLE public.bincontents (
    id integer NOT NULL,
    name character varying(250),
    price_kg numeric
);
    DROP TABLE public.bincontents;
       public         heap    postgres    false            �            1259    27520    bincontents_id_seq    SEQUENCE     �   CREATE SEQUENCE public.bincontents_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 )   DROP SEQUENCE public.bincontents_id_seq;
       public          postgres    false    203            "           0    0    bincontents_id_seq    SEQUENCE OWNED BY     I   ALTER SEQUENCE public.bincontents_id_seq OWNED BY public.bincontents.id;
          public          postgres    false    202            �            1259    27514    bintypes    TABLE     [   CREATE TABLE public.bintypes (
    id integer NOT NULL,
    name character varying(250)
);
    DROP TABLE public.bintypes;
       public         heap    postgres    false            �            1259    27512    bintypes_id_seq    SEQUENCE     �   CREATE SEQUENCE public.bintypes_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 &   DROP SEQUENCE public.bintypes_id_seq;
       public          postgres    false    201            #           0    0    bintypes_id_seq    SEQUENCE OWNED BY     C   ALTER SEQUENCE public.bintypes_id_seq OWNED BY public.bintypes.id;
          public          postgres    false    200            �            1259    27533    drivers    TABLE     Z   CREATE TABLE public.drivers (
    id integer NOT NULL,
    name character varying(250)
);
    DROP TABLE public.drivers;
       public         heap    postgres    false            �            1259    27531    drivers_id_seq    SEQUENCE     �   CREATE SEQUENCE public.drivers_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 %   DROP SEQUENCE public.drivers_id_seq;
       public          postgres    false    205            $           0    0    drivers_id_seq    SEQUENCE OWNED BY     A   ALTER SEQUENCE public.drivers_id_seq OWNED BY public.drivers.id;
          public          postgres    false    204            �            1259    27568    garbage_bins    TABLE     �   CREATE TABLE public.garbage_bins (
    id integer NOT NULL,
    filled_percent integer,
    type_id integer,
    content_id integer,
    user_id integer
);
     DROP TABLE public.garbage_bins;
       public         heap    postgres    false            �            1259    27566    garbage_bins_id_seq    SEQUENCE     �   CREATE SEQUENCE public.garbage_bins_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 *   DROP SEQUENCE public.garbage_bins_id_seq;
       public          postgres    false    211            %           0    0    garbage_bins_id_seq    SEQUENCE OWNED BY     K   ALTER SEQUENCE public.garbage_bins_id_seq OWNED BY public.garbage_bins.id;
          public          postgres    false    210            �            1259    27557 	   locations    TABLE     k   CREATE TABLE public.locations (
    id integer NOT NULL,
    location_x numeric,
    location_y numeric
);
    DROP TABLE public.locations;
       public         heap    postgres    false            �            1259    27555    locations_id_seq    SEQUENCE     �   CREATE SEQUENCE public.locations_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 '   DROP SEQUENCE public.locations_id_seq;
       public          postgres    false    209            &           0    0    locations_id_seq    SEQUENCE OWNED BY     E   ALTER SEQUENCE public.locations_id_seq OWNED BY public.locations.id;
          public          postgres    false    208            �            1259    27641    payments    TABLE     x   CREATE TABLE public.payments (
    id integer NOT NULL,
    payment_type character varying(250),
    user_id integer
);
    DROP TABLE public.payments;
       public         heap    postgres    false            �            1259    27639    payments_id_seq    SEQUENCE     �   CREATE SEQUENCE public.payments_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 &   DROP SEQUENCE public.payments_id_seq;
       public          postgres    false    219            '           0    0    payments_id_seq    SEQUENCE OWNED BY     C   ALTER SEQUENCE public.payments_id_seq OWNED BY public.payments.id;
          public          postgres    false    218            �            1259    27620    single_collections    TABLE     �   CREATE TABLE public.single_collections (
    id integer NOT NULL,
    weight numeric,
    time_collected timestamp without time zone,
    garbage_bin_id integer,
    tour_id integer,
    payment_id integer
);
 &   DROP TABLE public.single_collections;
       public         heap    postgres    false            �            1259    27618    single_collections_id_seq    SEQUENCE     �   CREATE SEQUENCE public.single_collections_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 0   DROP SEQUENCE public.single_collections_id_seq;
       public          postgres    false    217            (           0    0    single_collections_id_seq    SEQUENCE OWNED BY     W   ALTER SEQUENCE public.single_collections_id_seq OWNED BY public.single_collections.id;
          public          postgres    false    216            �            1259    27541    tours    TABLE     �   CREATE TABLE public.tours (
    id integer NOT NULL,
    date timestamp without time zone,
    tour_codes text,
    driverid integer
);
    DROP TABLE public.tours;
       public         heap    postgres    false            �            1259    27539    tours_id_seq    SEQUENCE     �   CREATE SEQUENCE public.tours_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 #   DROP SEQUENCE public.tours_id_seq;
       public          postgres    false    207            )           0    0    tours_id_seq    SEQUENCE OWNED BY     =   ALTER SEQUENCE public.tours_id_seq OWNED BY public.tours.id;
          public          postgres    false    206            �            1259    27604    users    TABLE     �   CREATE TABLE public.users (
    id integer NOT NULL,
    first_name character varying(250),
    last_name character varying(250),
    email character varying(250)
);
    DROP TABLE public.users;
       public         heap    postgres    false            �            1259    27602    users_id_seq    SEQUENCE     �   CREATE SEQUENCE public.users_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 #   DROP SEQUENCE public.users_id_seq;
       public          postgres    false    215            *           0    0    users_id_seq    SEQUENCE OWNED BY     =   ALTER SEQUENCE public.users_id_seq OWNED BY public.users.id;
          public          postgres    false    214            c           2604    27589    bin_tours id    DEFAULT     l   ALTER TABLE ONLY public.bin_tours ALTER COLUMN id SET DEFAULT nextval('public.bin_tours_id_seq'::regclass);
 ;   ALTER TABLE public.bin_tours ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    212    213    213            ^           2604    27525    bincontents id    DEFAULT     p   ALTER TABLE ONLY public.bincontents ALTER COLUMN id SET DEFAULT nextval('public.bincontents_id_seq'::regclass);
 =   ALTER TABLE public.bincontents ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    203    202    203            ]           2604    27517    bintypes id    DEFAULT     j   ALTER TABLE ONLY public.bintypes ALTER COLUMN id SET DEFAULT nextval('public.bintypes_id_seq'::regclass);
 :   ALTER TABLE public.bintypes ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    201    200    201            _           2604    27536 
   drivers id    DEFAULT     h   ALTER TABLE ONLY public.drivers ALTER COLUMN id SET DEFAULT nextval('public.drivers_id_seq'::regclass);
 9   ALTER TABLE public.drivers ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    205    204    205            b           2604    27571    garbage_bins id    DEFAULT     r   ALTER TABLE ONLY public.garbage_bins ALTER COLUMN id SET DEFAULT nextval('public.garbage_bins_id_seq'::regclass);
 >   ALTER TABLE public.garbage_bins ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    211    210    211            a           2604    27560    locations id    DEFAULT     l   ALTER TABLE ONLY public.locations ALTER COLUMN id SET DEFAULT nextval('public.locations_id_seq'::regclass);
 ;   ALTER TABLE public.locations ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    209    208    209            f           2604    27644    payments id    DEFAULT     j   ALTER TABLE ONLY public.payments ALTER COLUMN id SET DEFAULT nextval('public.payments_id_seq'::regclass);
 :   ALTER TABLE public.payments ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    219    218    219            e           2604    27623    single_collections id    DEFAULT     ~   ALTER TABLE ONLY public.single_collections ALTER COLUMN id SET DEFAULT nextval('public.single_collections_id_seq'::regclass);
 D   ALTER TABLE public.single_collections ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    216    217    217            `           2604    27544    tours id    DEFAULT     d   ALTER TABLE ONLY public.tours ALTER COLUMN id SET DEFAULT nextval('public.tours_id_seq'::regclass);
 7   ALTER TABLE public.tours ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    207    206    207            d           2604    27607    users id    DEFAULT     d   ALTER TABLE ONLY public.users ALTER COLUMN id SET DEFAULT nextval('public.users_id_seq'::regclass);
 7   ALTER TABLE public.users ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    214    215    215                      0    27586 	   bin_tours 
   TABLE DATA           8   COPY public.bin_tours (id, bin_id, tour_id) FROM stdin;
    public          postgres    false    213   {]       
          0    27522    bincontents 
   TABLE DATA           9   COPY public.bincontents (id, name, price_kg) FROM stdin;
    public          postgres    false    203   �]                 0    27514    bintypes 
   TABLE DATA           ,   COPY public.bintypes (id, name) FROM stdin;
    public          postgres    false    201   �]                 0    27533    drivers 
   TABLE DATA           +   COPY public.drivers (id, name) FROM stdin;
    public          postgres    false    205   �]                 0    27568    garbage_bins 
   TABLE DATA           X   COPY public.garbage_bins (id, filled_percent, type_id, content_id, user_id) FROM stdin;
    public          postgres    false    211   �]                 0    27557 	   locations 
   TABLE DATA           ?   COPY public.locations (id, location_x, location_y) FROM stdin;
    public          postgres    false    209   ^                 0    27641    payments 
   TABLE DATA           =   COPY public.payments (id, payment_type, user_id) FROM stdin;
    public          postgres    false    219   )^                 0    27620    single_collections 
   TABLE DATA           m   COPY public.single_collections (id, weight, time_collected, garbage_bin_id, tour_id, payment_id) FROM stdin;
    public          postgres    false    217   F^                 0    27541    tours 
   TABLE DATA           ?   COPY public.tours (id, date, tour_codes, driverid) FROM stdin;
    public          postgres    false    207   c^                 0    27604    users 
   TABLE DATA           A   COPY public.users (id, first_name, last_name, email) FROM stdin;
    public          postgres    false    215   �^       +           0    0    bin_tours_id_seq    SEQUENCE SET     ?   SELECT pg_catalog.setval('public.bin_tours_id_seq', 1, false);
          public          postgres    false    212            ,           0    0    bincontents_id_seq    SEQUENCE SET     A   SELECT pg_catalog.setval('public.bincontents_id_seq', 1, false);
          public          postgres    false    202            -           0    0    bintypes_id_seq    SEQUENCE SET     >   SELECT pg_catalog.setval('public.bintypes_id_seq', 1, false);
          public          postgres    false    200            .           0    0    drivers_id_seq    SEQUENCE SET     =   SELECT pg_catalog.setval('public.drivers_id_seq', 1, false);
          public          postgres    false    204            /           0    0    garbage_bins_id_seq    SEQUENCE SET     B   SELECT pg_catalog.setval('public.garbage_bins_id_seq', 1, false);
          public          postgres    false    210            0           0    0    locations_id_seq    SEQUENCE SET     ?   SELECT pg_catalog.setval('public.locations_id_seq', 1, false);
          public          postgres    false    208            1           0    0    payments_id_seq    SEQUENCE SET     >   SELECT pg_catalog.setval('public.payments_id_seq', 1, false);
          public          postgres    false    218            2           0    0    single_collections_id_seq    SEQUENCE SET     H   SELECT pg_catalog.setval('public.single_collections_id_seq', 1, false);
          public          postgres    false    216            3           0    0    tours_id_seq    SEQUENCE SET     ;   SELECT pg_catalog.setval('public.tours_id_seq', 1, false);
          public          postgres    false    206            4           0    0    users_id_seq    SEQUENCE SET     ;   SELECT pg_catalog.setval('public.users_id_seq', 1, false);
          public          postgres    false    214            t           2606    27591    bin_tours bin_tours_pkey 
   CONSTRAINT     V   ALTER TABLE ONLY public.bin_tours
    ADD CONSTRAINT bin_tours_pkey PRIMARY KEY (id);
 B   ALTER TABLE ONLY public.bin_tours DROP CONSTRAINT bin_tours_pkey;
       public            postgres    false    213            j           2606    27530    bincontents bincontents_pkey 
   CONSTRAINT     Z   ALTER TABLE ONLY public.bincontents
    ADD CONSTRAINT bincontents_pkey PRIMARY KEY (id);
 F   ALTER TABLE ONLY public.bincontents DROP CONSTRAINT bincontents_pkey;
       public            postgres    false    203            h           2606    27519    bintypes bintypes_pkey 
   CONSTRAINT     T   ALTER TABLE ONLY public.bintypes
    ADD CONSTRAINT bintypes_pkey PRIMARY KEY (id);
 @   ALTER TABLE ONLY public.bintypes DROP CONSTRAINT bintypes_pkey;
       public            postgres    false    201            l           2606    27538    drivers drivers_pkey 
   CONSTRAINT     R   ALTER TABLE ONLY public.drivers
    ADD CONSTRAINT drivers_pkey PRIMARY KEY (id);
 >   ALTER TABLE ONLY public.drivers DROP CONSTRAINT drivers_pkey;
       public            postgres    false    205            r           2606    27573    garbage_bins garbage_bins_pkey 
   CONSTRAINT     \   ALTER TABLE ONLY public.garbage_bins
    ADD CONSTRAINT garbage_bins_pkey PRIMARY KEY (id);
 H   ALTER TABLE ONLY public.garbage_bins DROP CONSTRAINT garbage_bins_pkey;
       public            postgres    false    211            p           2606    27565    locations locations_pkey 
   CONSTRAINT     V   ALTER TABLE ONLY public.locations
    ADD CONSTRAINT locations_pkey PRIMARY KEY (id);
 B   ALTER TABLE ONLY public.locations DROP CONSTRAINT locations_pkey;
       public            postgres    false    209            z           2606    27646    payments payments_pkey 
   CONSTRAINT     T   ALTER TABLE ONLY public.payments
    ADD CONSTRAINT payments_pkey PRIMARY KEY (id);
 @   ALTER TABLE ONLY public.payments DROP CONSTRAINT payments_pkey;
       public            postgres    false    219            x           2606    27628 *   single_collections single_collections_pkey 
   CONSTRAINT     h   ALTER TABLE ONLY public.single_collections
    ADD CONSTRAINT single_collections_pkey PRIMARY KEY (id);
 T   ALTER TABLE ONLY public.single_collections DROP CONSTRAINT single_collections_pkey;
       public            postgres    false    217            n           2606    27549    tours tours_pkey 
   CONSTRAINT     N   ALTER TABLE ONLY public.tours
    ADD CONSTRAINT tours_pkey PRIMARY KEY (id);
 :   ALTER TABLE ONLY public.tours DROP CONSTRAINT tours_pkey;
       public            postgres    false    207            v           2606    27612    users users_pkey 
   CONSTRAINT     N   ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_pkey PRIMARY KEY (id);
 :   ALTER TABLE ONLY public.users DROP CONSTRAINT users_pkey;
       public            postgres    false    215                       2606    27592    bin_tours bin_tours_bin_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.bin_tours
    ADD CONSTRAINT bin_tours_bin_id_fkey FOREIGN KEY (bin_id) REFERENCES public.garbage_bins(id);
 I   ALTER TABLE ONLY public.bin_tours DROP CONSTRAINT bin_tours_bin_id_fkey;
       public          postgres    false    2930    211    213            �           2606    27597     bin_tours bin_tours_tour_id_fkey    FK CONSTRAINT        ALTER TABLE ONLY public.bin_tours
    ADD CONSTRAINT bin_tours_tour_id_fkey FOREIGN KEY (tour_id) REFERENCES public.tours(id);
 J   ALTER TABLE ONLY public.bin_tours DROP CONSTRAINT bin_tours_tour_id_fkey;
       public          postgres    false    2926    213    207            }           2606    27579 )   garbage_bins garbage_bins_content_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.garbage_bins
    ADD CONSTRAINT garbage_bins_content_id_fkey FOREIGN KEY (content_id) REFERENCES public.bincontents(id);
 S   ALTER TABLE ONLY public.garbage_bins DROP CONSTRAINT garbage_bins_content_id_fkey;
       public          postgres    false    2922    203    211            |           2606    27574 &   garbage_bins garbage_bins_type_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.garbage_bins
    ADD CONSTRAINT garbage_bins_type_id_fkey FOREIGN KEY (type_id) REFERENCES public.bintypes(id);
 P   ALTER TABLE ONLY public.garbage_bins DROP CONSTRAINT garbage_bins_type_id_fkey;
       public          postgres    false    211    201    2920            ~           2606    27613 &   garbage_bins garbage_bins_user_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.garbage_bins
    ADD CONSTRAINT garbage_bins_user_id_fkey FOREIGN KEY (user_id) REFERENCES public.users(id);
 P   ALTER TABLE ONLY public.garbage_bins DROP CONSTRAINT garbage_bins_user_id_fkey;
       public          postgres    false    2934    215    211            �           2606    27647    payments payments_user_id_fkey    FK CONSTRAINT     }   ALTER TABLE ONLY public.payments
    ADD CONSTRAINT payments_user_id_fkey FOREIGN KEY (user_id) REFERENCES public.users(id);
 H   ALTER TABLE ONLY public.payments DROP CONSTRAINT payments_user_id_fkey;
       public          postgres    false    219    2934    215            �           2606    27629 9   single_collections single_collections_garbage_bin_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.single_collections
    ADD CONSTRAINT single_collections_garbage_bin_id_fkey FOREIGN KEY (garbage_bin_id) REFERENCES public.garbage_bins(id);
 c   ALTER TABLE ONLY public.single_collections DROP CONSTRAINT single_collections_garbage_bin_id_fkey;
       public          postgres    false    217    211    2930            �           2606    27652 5   single_collections single_collections_payment_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.single_collections
    ADD CONSTRAINT single_collections_payment_id_fkey FOREIGN KEY (payment_id) REFERENCES public.payments(id);
 _   ALTER TABLE ONLY public.single_collections DROP CONSTRAINT single_collections_payment_id_fkey;
       public          postgres    false    217    219    2938            �           2606    27634 2   single_collections single_collections_tour_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.single_collections
    ADD CONSTRAINT single_collections_tour_id_fkey FOREIGN KEY (tour_id) REFERENCES public.tours(id);
 \   ALTER TABLE ONLY public.single_collections DROP CONSTRAINT single_collections_tour_id_fkey;
       public          postgres    false    2926    217    207            {           2606    27550    tours tours_driverid_fkey    FK CONSTRAINT     {   ALTER TABLE ONLY public.tours
    ADD CONSTRAINT tours_driverid_fkey FOREIGN KEY (driverid) REFERENCES public.drivers(id);
 C   ALTER TABLE ONLY public.tours DROP CONSTRAINT tours_driverid_fkey;
       public          postgres    false    2924    207    205                  x������ � �      
      x������ � �            x������ � �            x������ � �            x������ � �            x������ � �            x������ � �            x������ � �            x������ � �            x������ � �     